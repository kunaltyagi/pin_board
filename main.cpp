#include <pin_board.hpp>

using namespace pin_board;

template <class T>
constexpr auto has_adc() {
    return has_alternative<ADC>(T{});
}
template <PinSizeT pin_no, class Layout>
constexpr auto has_adc() {
    return has_adc<std::tuple_element_t<pin_no, Layout>>();
}

template <class T>
using HasADC = std::enable_if_t<has_adc<T>(), bool>;
template <PinSizeT pin_no, class Layout>
using HasADCNum = std::enable_if_t<has_adc<pin_no, Layout>(), bool>;

// init and layout are coupled for obvious reasons
auto pin_layout_simple =
    make_board<UnsafePin<PWM, V_GND>, UnsafePin<PWM, ADC>>();

template <class Layout>
void simple_init(Layout& pin_layout) {
    std::get<0>(pin_layout).state = PWM{};
    std::get<1>(pin_layout).state = ADC{};
    // std::get<1>(pin_layout_simple).state = V_GND{};  // compile error
}

// this time, the sequence doesn't matter
using Pin0 = Pin<0, PWM, V_GND>;
using Pin1 = Pin<1, PWM, ADC>;

auto pin_layout_named = make_board(Pin1{}, Pin0{});

template <class Layout>
void named_init(Layout& pin_layout) {
    std::get<Pin0>(pin_layout).state = PWM{};
    std::get<Pin1>(pin_layout).state = ADC{};
    // std::get<Pin1>(pin_layout_simple).state = V_GND{};  // compile error
}

template <class Pin, class Layout, HasADC<Pin> = true>
bool adc_status(const Layout& pin_layout) {
    if (holds_alternative<ADC>(std::get<Pin>(pin_layout))) {
        return true;
    }
    return false;
}
template <PinSizeT pin_no, class Layout, HasADCNum<pin_no, Layout> = true>
bool adc_status(const Layout& pin_layout) {
    return adc_status<std::tuple_element_t<pin_no, Layout>>(pin_layout);
}

int main() {
    simple_init(pin_layout_simple);
    named_init(pin_layout_named);

    return adc_status<1>(pin_layout_simple) &&
           adc_status<Pin1>(pin_layout_named);
}