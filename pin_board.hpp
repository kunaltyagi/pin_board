#pragma once

#include <tuple>
#include <variant>

namespace pin_board {
struct Capability {};

// Technically, no need to derive except to have a common functionality
struct NONE : Capability {};
struct PWM : Capability {};
struct V_GND : Capability {};
struct ADC : Capability {};

using PinSizeT = std::uint8_t;

template <PinSizeT pin_no, class... Types>
struct Pin;

template <class... Types>
struct UnsafePin {
    using CapabilityVariant = std::variant<Types...>;

    template <PinSizeT pin_no>
    using safe_pin_t = Pin<pin_no, Types...>;

    CapabilityVariant state = {};
    constexpr operator CapabilityVariant() const noexcept { return state; }

  protected:
    constexpr UnsafePin() noexcept {}
    constexpr UnsafePin(const CapabilityVariant& var) noexcept : state(var) {}
};

template <PinSizeT pin_no, class... Types>
struct Pin : UnsafePin<Types...> {
    using Base = UnsafePin<Types...>;
    using CapabilityVariant = typename Base::CapabilityVariant;

    using Base::Base;
    constexpr Pin() noexcept = default;
    constexpr Pin(const Pin&) noexcept = default;
    constexpr Pin(const Types&... var) noexcept : Pin(){};
    constexpr Pin(const CapabilityVariant& var) noexcept : state(var) {}

    CapabilityVariant state = {};
};

template <PinSizeT pin_no, class... Types>
constexpr auto make_pin(const Types&... args) noexcept -> decltype(auto) {
    return Pin<pin_no, Types...>();
}

namespace detail {
template <PinSizeT pin_no>
constexpr auto make_board_helper() noexcept -> decltype(auto) {
    return std::tuple<>();
}
template <PinSizeT pin_no, class UnsafePin, class... UnsafePins>
constexpr auto make_board_helper() noexcept -> decltype(auto) {
    return std::tuple_cat(
        std::tuple<typename UnsafePin::template safe_pin_t<pin_no>>{},
        make_board_helper<pin_no + 1, UnsafePins...>());
}
}  // namespace detail

template <class UnsafePin, class... UnsafePins>
constexpr auto make_board() noexcept -> decltype(auto) {
    return detail::make_board_helper<0, UnsafePin, UnsafePins...>();
}

constexpr auto make_board() noexcept -> decltype(auto) {
    return std::tuple<>();
}
template <class Pin, class... Pins>
constexpr auto make_board(Pin pin, Pins... pins) noexcept -> decltype(auto) {
    return std::tuple_cat(std::tuple<Pin>{}, make_board(pins...));
}

template <class T, class C, class = std::void_t<typename C::CapabilityVariant>>
constexpr auto holds_alternative(const C& v) noexcept {
    return std::holds_alternative<T>(v.state);
}

namespace detail {
template <std::size_t VariantSize, class Alternative, class Variant>
constexpr auto has_alternative() {
    return false;
}
template <std::size_t VariantSize,
          class Alternative,
          class Variant,
          class Type,
          class... Types>
constexpr auto has_alternative() {
    if constexpr (std::is_same_v<
                      Alternative,
                      std::variant_alternative_t<VariantSize, Variant>>) {
        return true;
    } else if constexpr (VariantSize == 0) {
        return false;
    }
    return has_alternative<VariantSize - 1, Alternative, Variant, Types...>();
}
}  // namespace detail
template <class Alternative, PinSizeT pin_no, class... Types>
constexpr auto has_alternative(const Pin<pin_no, Types...>& pin) {
    return detail::has_alternative<std::variant_size_v<std::variant<Types...>> -
                                       1,
                                   Alternative,
                                   std::variant<Types...>,
                                   Types...>();
}
}  // namespace pin_board